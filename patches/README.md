cpf_mem_type.patch - deemed unusable as per 5.75 https://github.com/civicrm/civicrm-core/pull/30114 (need to make changes in custom templates as per token changes - check upgrade message)
added patch 30244.patch for cpf receipt templates
24645.patch - merged in 5.55.0 - https://projects.cividesk.com/projects/4/tasks/5161
23021.patch - merged in 5.49.0 -  needed for UserDashboard extension 

REST authentication for Drupal 9:
* df9b24b2fcf2c2603380834b289e8460e80a7e5c.patch
authx - Allow civicrm/ajax/rest to authenticate via API key
Note: the patch is modified to remove all 'pipe' references
* rest-x-*.patch files
rest - allow authx authentication for API calls



| Patch | Description | Cividesk task/ticket | Merge Status |
| 25282.patch | Dummy payment processor should be flagged as such on LIVE page | https://projects.cividesk.com/projects/3/tasks/archive?modal=Task-5137-3 | merged later than 5.59.0 |
