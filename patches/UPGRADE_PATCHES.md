| Patch | Description | Cividesk task/ticket | Merge Status |
|---------|---------|---------|---------|
| 22804.patch | Implement new UI for configuring dedupe rule usage | https://projects.cividesk.com/projects/3/tasks/5341 |merged later than 5.49 |
| 25723.patch | Convert html special chars to their characters in ical file generation| https://projects.cividesk.com/projects/29/tasks/5291 ||
| 25629.patch | Add filter on pledge payment in contribution report template | https://projects.cividesk.com/projects/109/activity?modal=Task-5277-109 | merged later than 5.60 |
| 25687.patch | Past campaigns are not to be assigned via batch update | https://projects.cividesk.com/projects/9?modal=Task-5297-9 |  merged later than 5.61 |
| 24640-adapted.patch | Fix custom data view for case of type Money | https://projects.cividesk.com/projects/109/tasks/5093 | merged later than 5.55.0 |
| 24776.patch | Need to increase data size for 'url' column on 'civicrm_website' table | https://projects.cividesk.com/projects/93/tasks/archive?modal=Task-5189-93 | merged later than 5.56.0 |
| osticket_11233.patch | Smart group contacts from custom search show wrong results | https://lab.civicrm.org/dev/core/-/issues/4091 ||
| 25487.patch | Fix the id help instead use class help | https://projects.cividesk.com/projects/29/tasks/archive?modal=Task-5222-29 | merged later than 5.60 |
| 24492.patch | Pledge status is missing on View Pledge page | https://projects.cividesk.com/projects/109/tasks/archive?modal=Task-5103-109 | merged later than 5.53 |
| 24598.patch | Fix css for price fields of html type Select with long labels | https://projects.cividesk.com/projects/12/tasks/archive?modal=Task-5169-12 | merged later than 5.57 |
| report_improvements.patch | Remove pledge status as mandatory in Pledge report | https://github.com/civicrm/civicrm-core/pull/24028 | merged later than 5.53 |
| report_improvements.patch | Fix 'Pledge Made Date' display to show date and not time | https://github.com/civicrm/civicrm-core/pull/24031 | merged later than 5.53 |
| report_improvements.patch | Remove start/end date as mandatory in Demographics report | https://github.com/civicrm/civicrm-core/pull/24048 | merged later than 5.53 |
| report_improvements.patch | Expose city in case detail report | https://github.com/civicrm/civicrm-core/pull/24109 | merged later than 5.53 |
| report_improvements.patch | Add filter for 'Activity type of the last activity' for case detail report | https://github.com/civicrm/civicrm-core/pull/24120 | merged later than 5.53 |
| report_improvements.patch | Expose contact custom data as columns in case detail report | https://github.com/civicrm/civicrm-core/pull/24155 | merged later than 5.53 |
| permission.patch | Fix permission issue || already in core |
| 24777.patch | Write-off feature for pledges | https://projects.cividesk.com/projects/109/tasks/5100 ||
| pledge_payment_filter_on_contri_search.patch | Search on pledge payments on Find contributions screen | https://lab.civicrm.org/dev/core/-/issues/3872 ||
| 25807.patch | Add pledge letters feature | https://lab.civicrm.org/dev/core/-/issues/4061 https://projects.cividesk.com/projects/109/tasks/archive?modal=Task-5240-109 ||
| event_offline_workflow_tpl.patch | Expose tplParams for event id/fields in offline registrations | https://projects.cividesk.com/projects/3/tasks/5303 (dependency https://github.com/cividesk/com.cividesk.event.callinks/commit/a41c25332f14f8b9559f7ab9676af7df52d2443c) | revisit in upgraded version |
| 22800.patch | Mailing Report: do not recalculate the recipients when the count is zero | https://github.com/civicrm/civicrm-core/pull/22800 | merged in 5.48.0 |
| 22264.patch | Fix Fatal Error on contribution tab for recurring | https://github.com/civicrm/civicrm-core/pull/22264 | merged in 5.47.0 |
| alh_wrench.patch | dev/core#1821 fix custom field option list radio clear link | https://github.com/civicrm/civicrm-core/pull/26110 | merged in 5.62.0 |
| osticket_12187.patch | for recurring ensure new contributions are created using the new amount, not the amount from the template or initial contribution. | https://projects.cividesk.com/projects/95?modal=Task-5919-95 | fixed in 5.52.0 |