<?php
/**
 * This file auto-generates a composer.json from the patches directory
 *
 * Note: not best practices as we are copying the patches locally rather than using URL
 *       ie. https://patch-diff.githubusercontent.com/raw/civicrm/civicrm-core/pull/23021.patch
 *       but this is necessary as we also have to support patching from local for the deploy repo
 */
?>
{
  "name": "cividesk/civicrm",
  "description": "CiviCRM platform for Cividesk.",
  "version": "<?php echo $argv[1]; ?>",
  "require": {
    "cweagans/composer-patches": "~1.0",
    "civicrm/civicrm-core": "<?php echo $argv[2]; ?>",
    "civicrm/civicrm-drupal-8": "<?php echo $argv[2]; ?>",
    "civicrm/civicrm-packages": "<?php echo $argv[2]; ?>"
  },
  "extra": {
    "patches": {
      "civicrm/civicrm-core": {
<?php
$list = '';
chdir(__DIR__ . '/patches/');
foreach (glob('*.patch') as $file) {
  $list .= '        "' . $file . '":"vendor/cividesk/civicrm/patches/' . $file . '",' . PHP_EOL;
}
// Remove the comma on the last item of the list
$list = substr($list, 0, -2) . PHP_EOL;
echo $list;
?>
      },
      "civicrm/civicrm-packages": {
<?php
$list = '';
chdir(__DIR__ . '/patches/packages/');
foreach (glob('*.patch') as $file) {
  $list .= '        "' . $file . '":"vendor/cividesk/civicrm/patches/packages/' . $file . '",' . PHP_EOL;
}
// Remove the comma on the last item of the list
$list = substr($list, 0, -2) . PHP_EOL;
echo $list;
?>
      }
    },
    "composer-exit-on-patch-failure": true
  }
}
