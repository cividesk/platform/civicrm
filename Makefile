# when changing below, please also change in:
# - .gitlab-ci.yml
BRANCH = 5.75
VERSION = 5.75.2
GITTAG = $(shell git log -1 --pretty='format:%h (%cs)')

# change as appropriate, but beware this will be obliterated by make clean, make build, ...
CACHE = cache-dir
BUILD = build-dir

CORE_FILE = civicrm-$(VERSION)-drupal.tar.gz
CORE_DIST = https://download.civicrm.org/$(CORE_FILE)
CORE_REPO = https://github.com/civicrm/civicrm-core.git
PATCHES = $(shell pwd)/patches

GIT_OPTS = -c advice.detachedHead=false --quiet

.PHONY: all clean wipe build secure

all: clean build composer

clean:
	rm -Rf $(BUILD) vendor

wipe: clean
	rm -Rf $(CACHE)

build: $(BUILD) build-patch identify secure wordpress

$(BUILD): $(CACHE)/$(CORE_FILE)
	mkdir -p $(BUILD)
	cd $(BUILD) && cat ../$(CACHE)/$(CORE_FILE) | tar xfz -

$(CACHE)/$(CORE_FILE):
	mkdir -p $(CACHE)
	cd $(CACHE) && wget -q -O $(CORE_FILE) $(CORE_DIST)

.SILENT: build-patch
build-patch:
	cd $(BUILD)/civicrm && \
	for file in $(PATCHES)/*.patch; do \
	  echo Applying $$file; \
          patch -p1 --quiet -N < $$file; \
	done
	cd $(BUILD)/civicrm/packages && \
	for file in $(PATCHES)/packages/*.patch; do \
	  echo Applying $$file; \
          patch -p1 --quiet -N < $$file; \
	done

identify:
	cd $(BUILD)/civicrm && \
	  sed -i "s/version}</version}-$(GITTAG)</g" templates/CRM/common/footer.tpl
	# TODO: create a patch and add it to the patches folder ...
	# so composer-based installs also have GITTAG in footer

secure:
	cd $(BUILD) && \
	  rm -f civicrm/vendor/pear/log/README.rst && \
	  find . -type d -exec chmod 755 {} + && \
	  find . -type f -exec chmod go-w {} +

wordpress:
	cd $(BUILD) && \
	  wget -O - https://github.com/civicrm/civicrm-wordpress/archive/refs/tags/$(VERSION).tar.gz | tar xfz - && \
	  mv civicrm-wordpress-$(VERSION) wp-civicrm && \
	  cp -Rfp civicrm wp-civicrm/ && rm -Rf wp-civicrm/civicrm/drupal

composer: # regenerate every time as it depends on so many things ...
	php composer.php $(BRANCH) $(VERSION) > composer.json

# Additional targets for building Docker images

build-extend: # do NOT depend on $(BUILD) to catch pipeline issues
	cd $(BUILD) && git clone -b $(BRANCH) https://gitlab.com/cividesk/deploy/civicrm-extend.git && rm -Rf civicrm-extend/.git

l10n:
	wget -O - https://download.civicrm.org/civicrm-$(VERSION)-l10n.tar.gz | tar xfz -

