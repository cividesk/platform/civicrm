ARG BUILD_REGISTRY
FROM ${BUILD_REGISTRY}cividesk/drupal:7.x

# Install CiviCRM utilities
ADD https://download.civicrm.org/cv/cv.phar /usr/local/bin/cv
ADD https://download.civicrm.org/civix/civix.phar /usr/local/bin/civix
RUN chmod +rx /usr/local/bin/cv /usr/local/bin/civix

# All instance setup done as user www-data for ownership/permissions
USER www-data

# install Cividesk utilities
ADD --chown=www-data:www-data ./sso /opt

# --chown is necessary as otherwise all is owned by root
ADD --chown=www-data:www-data ./civicrm /var/www/html/web/sites/all/modules/civicrm

# Do not forget to switch back for the startup scripts
USER root
