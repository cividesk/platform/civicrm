# CiviCRM patches

## Getting started

### Drupal 9: with composer
```
composer require cividesk/civicrm:5.75.x-dev
```
You can even then remove the ```civicrm/civicrm-core```, ```civicrm/civicrm-drupal-8``` and ```civicrm/civicrm-packages``` lines from your ```composer.json``` file as these dependencies are already included in ```cividesk/civicrm-patches```.

### Drupal 7 / WordPress: in Dockerfile from container
```
ARG BRANCH=5.75
FROM ${BUILD_REGISTRY}cividesk/civicrm:${BRANCH} AS civicrm
FROM (your base container)
COPY --chown=www-data:www-data --from=civicrm civicrm (target path for civicrm)
COPY --chown=www-data:www-data --from=civicrm civicrm-extend (target path for civicrm-extend)
```

## Add new patches to the build

If the patch is available in a gitlab repository:
- [ ] download the patch in the patches folder (use the same name as the commit id)
  - the easiest way to download a patch file is to add ```.patch``` to the end of the URL for the corresponding PR.
  - use the same name as the commit id for the patch file

If you need to create patch an existing filei (ie. CRM/Report/Form.php):
```
cd {civicrm_root}
cp {file}.php {file}.php.ori
vi {file}.php
diff -Naur {file}.php.ori {file}.php > {patches_dir}/my-new-patch.patch
```

Then:
- [ ] edit the ```patches/README.md``` file to add a comment and instructions about your patch
- [ ] run ```make``` and check that all patches apply correctly
- [ ] run ```make release && git status```
- [ ] check that all looks good (the composer.json should have changed)
- [ ] commit and push all your changes
